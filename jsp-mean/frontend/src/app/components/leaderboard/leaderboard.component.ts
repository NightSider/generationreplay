import { Component, OnInit } from '@angular/core';
import { SongService } from '../../song.service';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})
export class LeaderboardComponent implements OnInit {
  users: any;

  constructor(private songservice: SongService) {

  }

  ngOnInit() {
    this.songservice.getUsers().subscribe(res => {
      var userScores = res["users"];
      var userScoresRounded = userScores.map(el => {return {userName: el.userName, ratio: Math.round(el.ratio*100)/100};});
      userScoresRounded.sort((a, b) => {
        if(a.ratio > b.ratio){
          return -1;
        }
        else if(a.ratio < b.ratio){
          return 1;
        }
        else{
          return 0;
        }
      });
      this.users = userScoresRounded;
    });
  }

}
