import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayhubComponent } from './playhub.component';

describe('PlayhubComponent', () => {
  let component: PlayhubComponent;
  let fixture: ComponentFixture<PlayhubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayhubComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayhubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
