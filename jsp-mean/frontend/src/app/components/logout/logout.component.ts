import { Component, OnInit, NgZone} from '@angular/core';
import { LoginService } from '../../login.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private loginservice: LoginService, private router: Router, private zone:NgZone) { }

  logout(){
    this.loginservice.logout();
  }

  ngOnInit() {
  }

}
