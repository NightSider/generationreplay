import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, NgZone } from '@angular/core';
import { SongService } from '../../song.service';
import { LoginService } from '../../login.service';
import {Router} from "@angular/router";


@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayComponent implements OnInit {

  genres: string[];

  constructor(private songservice: SongService, private loginservice: LoginService, private cdRef:ChangeDetectorRef, private router: Router, private zone:NgZone){ }

  addSong(genre){
    var guess;
    var playList = this.getPlaylist();

    var previewURI = document.getElementById('source').getAttribute("src");
    var songId = playList.find(song => song.uri == previewURI).spotifySongId;
    console.log(songId);
    this.loginservice.addSong(songId, genre).subscribe(res => {
      if(res["status"] == 201){
        console.log("new song added");
        return;
      }
      guess = res["guess"];
      //this.getNextTrack();
      //this.zone.run(() => this.router.navigate(['/success']));
        if(guess == true){
          this.zone.run(() => this.router.navigate(['/success']));

        }else{
          this.zone.run(() => this.router.navigate(['/loose']));
        }


    });
  }

  findSong(song_get){
    this.songservice.findSong(song_get).subscribe(res => console.log(res));
  }

  findAlbumById(album_id){
    this.songservice.findAlbumById(album_id);
  }


  getNextTrack(){
    if(getGuessed()){
      console.log("I'm in")
      this.zone.run(() => this.router.navigate(['/postgame']));
    }else{
      if(count((JSON.parse(getSonglistFromServer())).tracks)==0){
        changeSonglistOnServer(toArray(getFiveRandomSongs()));
        console.log("Songlist was refilled");
      }
      var songsFromServerJSON = JSON.parse(getSonglistFromServer());
      var array = toArray(songsFromServerJSON.tracks);
      console.log("length: "+ array.length)
      array.forEach(element => {
        console.log("artist: " + element.artist)
      });
      var arrayShifted = array.slice(1);
      oneMoreGuess();
      var elementToPlay = songsFromServerJSON.tracks[0].uri;
      changeSonglistOnServer(arrayShifted)

      var audio = document.getElementById('audioController') as HTMLInputElement
      audio.setAttribute("audiosource", elementToPlay);
      return elementToPlay;
    }

    function count(songs){
      var count=0;
      for(var prop in songs) {
         if (songs.hasOwnProperty(prop)) {
            ++count;
         }
      }
      return count;
    }

    function toArray(json){
      var result = [];
      for(var i in json){
        result.push(json[i]);
      }
      return result;
    }

    function oneMoreGuess(){
      var xhr = new XMLHttpRequest();
      xhr.open("POST", "http://localhost:3000/api/guessed", false);
      xhr.onload = function (e) {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
          } else {
            console.error("Status is " + xhr.status)
            console.error("Readystate is " + xhr.readyState)
          }
        }
      };
      xhr.onerror = function (e) {
        console.error("Statustext on error: " + xhr.statusText);
      };
      xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
      xhr.send(null);
    }

    function getFiveRandomSongs(){
      var xhr = new XMLHttpRequest();
      xhr.open("GET", "http://localhost:3000/api/next_track", false);
      xhr.onload = function (e) {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
        }   else {
            console.error(xhr.statusText);
          }
        }
      };
      xhr.onerror = function (e) {
        console.error(xhr.statusText);
      };
      xhr.send(null);
      var songsJSON = JSON.parse(xhr.responseText);
      var fiveSongList = [];
      for(var songNumber = 0; songNumber < 20; songNumber++){
        var songToAdd = songsJSON.songURIs[Math.floor(Math.random() * (songsJSON.songURIs.length))];
        if(!fiveSongList.includes(songToAdd)){
          fiveSongList.push(songToAdd);
        }
        if(fiveSongList.length == 5){
          var xhr2 = new XMLHttpRequest();
          xhr2.open("POST", "http://localhost:3000/js/gethex", false);
          xhr2.onload = function (e) {
            if (xhr2.readyState === 4) {
              if (xhr2.status === 200) {
              } else {
                console.error("Status is " + xhr2.status)
                console.error("Readystate is " + xhr2.readyState)
              }
            }
          };
          xhr2.onerror = function (e) {
            console.error("Statustext on error: " + xhr2.statusText);
          };
          var toSend = JSON.stringify(fiveSongList);
          xhr2.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
          xhr2.send(toSend);
          return fiveSongList;
        }
      }
    }

    function getGuessed(){
      var xhr = new XMLHttpRequest();
      xhr.open("GET", "http://localhost:3000/api/get_guessed", false);
      xhr.onload = function (e) {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
        }   else {
            console.error(xhr.statusText);
          }
        }
      };
      xhr.onerror = function (e) {
        console.error(xhr.statusText);
      };
      xhr.send(null);
      var songsGuessed = JSON.parse(xhr.responseText);
      return songsGuessed.flag;
    }

    function changeSonglistOnServer(songsToChange){
      var xhr = new XMLHttpRequest();
      xhr.open("POST", "http://localhost:3000/api/change_Round_Tracks", false);
      xhr.onload = function (e) {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
          } else {
            console.error("Status is " + xhr.status)
            console.error("Readystate is " + xhr.readyState)
          }
        }
      };
      xhr.onerror = function (e) {
        console.error("Statustext on error: " + xhr.statusText);
      };
      var toSend = JSON.stringify(songsToChange);
      xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
      xhr.send(toSend);
    }

    function getSonglistFromServer(){
      var xhr = new XMLHttpRequest
      xhr.open("GET", "http://localhost:3000/api/get_Round_Tracks", false);
      xhr.onload = function (e) {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
          } else {
            console.error(xhr.statusText);
          }
        }
      };
      xhr.onerror = function (e) {
        console.error(xhr.statusText);
      };
      xhr.send(null);
      // console.log("Response: " + xhr.responseText);
      return xhr.responseText;
    }
  }

  getPlaylist(){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://localhost:3000/api/next_track", false);
    xhr.onload = function (e) {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          //console.log(xhr.responseText);
        } else {
          console.error(xhr.statusText);
        }
      }
    };
    xhr.onerror = function (e) {
      console.error(xhr.statusText);
    };
    xhr.send(null);
    var xhrJSON = JSON.parse(xhr.responseText);
    return xhrJSON.songURIs;
  }

  ngAfterViewInit(){
  }

  ngOnInit() {
    //this.genres = ["Metal", "Pop", "Rock", "Rap", "Hip Hop", "Deathmetal",
    //  "Technical Death Metal", "Melodic Death Metal", "Country", "Blackmetal"];
    this.genres = ["Pop", "Pop Punk", "Rap", "Trap", "Pop Rock", "Alternative Rock", "Classic Rock", "Classic Pop", "Latin Pop", "Indie Pop", "Alternative Metal", "Rock", "Grunge", "Hard Rock", "Reggaeton", "Indie Folk", "Electro House", "Nu Metal", "Gangster Rap", "Garage Rock", "Folk Rock", "Soul", "Underground Hip-Hop", "Psychedelic Rock", "Hip-Hop", "R&B", "EDM", "Latin", "Electro", "Metal", "Death Metal", "Hardcore", "Metalcore", "Deathcore", "Deep House", "Acoustic Pop", "Rock&Roll", "Folk", "Country", "Punk", "Indie", "Techno", "Funk", "Blues", "Jazz", "Classical", "House", "Singer-Songwriter", "Alternative", "Swing", "Reggae", "Schlager", "Disco", "Trance", "Ska", "Drum&Bass", "Hardstyle", "Viking Metal", "Thrash Metal", "Black Metal", "Power Metal", "Punk Rock"].sort();

    this.cdRef.detectChanges();
   }

  ngOnChange(){
  }

}
