import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedplayComponent } from './advancedplay.component';

describe('AdvancedplayComponent', () => {
  let component: AdvancedplayComponent;
  let fixture: ComponentFixture<AdvancedplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvancedplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
