import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private loginservice: LoginService) { }

  // hier kommen Konstgruktoren der Methoden


  login(){
    this.loginservice.login();
  }

  trycors(){
    this.loginservice.trycors();
  }

  ngOnInit() {
    window.open('http://localhost:3000/api/login', '', 'width=400,height=700');
  }

}
