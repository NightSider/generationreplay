import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-loose',
  templateUrl: './loose.component.html',
  styleUrls: ['./loose.component.css']
})
export class LooseComponent implements OnInit {

  constructor(private router: Router) { }

  guessAgain(){
    this.router.navigate(['/play'])
  }

  ngOnInit() {
  }

}
