import { Component, OnInit } from '@angular/core';
import { JavascriptService } from '../../javascript.service';
import { SongService } from '../../song.service';

@Component({
  selector: 'app-postgame',
  templateUrl: './postgame.component.html',
  styleUrls: ['./postgame.component.css']
})
export class PostgameComponent implements OnInit {

  constructor(private javascriptservice: JavascriptService, private songservice: SongService) { }

  create_hexagon(){
    var hexagon = {
        bg_default_color: '#99ccff',
        value_default_color: '#ffff99',
        x_offset: 30,
        ssin: function(degree) {
            return Math.sin(degree * Math.PI / 180);
        },
        init: function(id, side_length, names, color) {
            this.side_length = side_length;
            this.hexagon = document.getElementById(id);
            this.hexagon.width = this.side_length * 2 * this.ssin(60) + this.x_offset * 2;
            this.hexagon.height = this.side_length * 2;

            var width = this.hexagon.width;
            var height = this.hexagon.height;
            var S = this.x_offset;

            if (typeof (color) === 'undefined') {
                color = this.bg_default_color;
            }
            //JUL TODO
            var canvas = document.getElementById(id);
            canvas['width'] = canvas['width'] * 1.3;
            canvas['height'] = canvas['height'] * 1.3;
            var hexagoncontext = this.hexagon.getContext('2d');
            hexagoncontext.fillStyle = color;
            hexagoncontext.strokeStyle = color;
            hexagoncontext.font= "15px Arial";
            hexagoncontext.beginPath();
            hexagoncontext.moveTo(width / 2 + 20, 20);
            hexagoncontext.lineTo(width - S  + 20, height / 4 + 20);
            hexagoncontext.lineTo(width - S  + 20, height * 3 / 4 + 20);
            hexagoncontext.lineTo(width / 2 + 20, height + 20);
            hexagoncontext.lineTo(S + 20, height * 3 / 4 + 20);
            hexagoncontext.lineTo(S + 20, height / 4 + 20);
            hexagoncontext.lineTo(width / 2 + 20, 20);
            hexagoncontext.stroke();
            hexagoncontext.fill();
            hexagoncontext.fillText(names[0], width / 2 + S / 2 - 10, 15);
            hexagoncontext.fillText(names[1], width - S + 25, height / 4 + 15);
            hexagoncontext.fillText(names[2], width - S + 25, height * 3 / 4 + 35);
            hexagoncontext.fillText(names[3], width / 2 + S / 2 - 20, height + 35);
            hexagoncontext.fillText(names[4], 0, height * 3 / 4 + 35);
            hexagoncontext.fillText(names[5], 0, height / 4 + 15);

        },
        draw: function(values, color) {
            if (values.length < 6) {
                return false;
            }

            for (var i in values) {
                values[i] = parseFloat(values[i]);

                if (values[i] > 1 || values[i] < 0) {
                    return false;
                }
            }

            if (typeof (color) === 'undefined') {
                color = this.value_default_color;
            }

            var width = this.hexagon.width;
            var L = this.side_length;
            var S = this.x_offset;
            var V = values;

            var hexagoncontext = this.hexagon.getContext('2d');
            hexagoncontext.fillStyle = color;
            hexagoncontext.strokeStyle = color;
            hexagoncontext.beginPath();
            hexagoncontext.moveTo(width / 2 - 15, L * (1 - V[0]) + 20);
            hexagoncontext.lineTo(this.ssin(60) * L * (1 + V[1]) + S + 20, (1 - V[1] / 2) * L + 20);
            hexagoncontext.lineTo(this.ssin(60) * L * (1 + V[2]) + S + 20, (1 + V[2] / 2) * L + 20);
            hexagoncontext.lineTo(width / 2 - 15, (1 + V[3]) * L + 20);
            hexagoncontext.lineTo(this.ssin(60) * L * (1 - V[4]) + S + 20, (1 + V[4] / 2) * L + 20);
            hexagoncontext.lineTo(this.ssin(60) * L * (1 - V[5]) + S + 20, (1 - V[5] / 2) * L + 20);
            hexagoncontext.lineTo(width / 2 - 15, L * (1 - V[0]) + 20);
            hexagoncontext.stroke();
            hexagoncontext.fill();
        }
      }


      // var xhr = new XMLHttpRequest();
      // xhr.open("GET", "http://localhost:3000/api/get_Round_Tracks", false);
      // xhr.onload = function (e) {
      //   if (xhr.readyState === 4) {
      //     if (xhr.status === 200) {
      //   }   else {
      //       console.error(xhr.statusText);
      //     }
      //   }
      // };
      // xhr.onerror = function (e) {
      //   console.error(xhr.statusText);
      // };
      // xhr.send(null);
      // var songsJSON = JSON.parse(xhr.responseText);
      // console.log(songsJSON);
      // var hex_arr = [];
      //
      // for(var i = 0; i<5; i++){
      //   console.log(songsJSON.allTracks[i]);
      //   this.songservice.get_genres_for_hex(songsJSON.allTracks[i].spotifySongId).subscribe(res =>
      //   {
      //     console.log(res);
      //     hex_arr.push(res);
      //     });
      // }
      hexagon.init('hex_1', 100, ["Rock", "Pop", "Metal", "Indie", "Funk", "Rap"], '#675463');
      hexagon.draw([0.2,0.6,0.4,0.9,0.1,0.5], '#848484');
      hexagon.init('hex_2', 100, ["Rock", "Pop", "Metal", "Indie", "Funk", "Rap"], '#675463');
      hexagon.draw([0.2,0.2,0.4,0.1,0.7,0], '#848484');
      hexagon.init('hex_3', 100, ["Rock", "Pop", "Metal", "Indie", "Funk", "Rap"], '#675463');
      hexagon.draw([0.9,0.2,0.4,0.4,0.1,0.5], '#848484');
      hexagon.init('hex_4', 100, ["Rock", "Pop", "Metal", "Indie", "Funk", "Rap"], '#675463');
      hexagon.draw([0.6,0.6,0.4,0.7,0.1,0.5], '#848484');
      hexagon.init('hex_5', 100, ["Rock", "Pop", "Metal", "Indie", "Funk", "Rap"], '#675463');
      hexagon.draw([0.2,0.8,0.4,0.3,0.4,0.5], '#848484');
      // var names = hex_arr.genres.map(e => {return e.name;});
      // var counters = hex_arr.genres.map(e => {return e.counter;})
    }


    get_hex_values(){
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "http://localhost:3000/api/get_Round_Tracks", false);
        xhr.onload = function (e) {
          if (xhr.readyState === 4) {
            if (xhr.status === 200) {
          }   else {
              console.error(xhr.statusText);
            }
          }
        };
        xhr.onerror = function (e) {
          console.error(xhr.statusText);
        };
        xhr.send(null);
        var songsJSON = JSON.parse(xhr.responseText);
        console.log(songsJSON);
        var hex_arr = [];

        for(var i = 0; i<5; i++){
          console.log(songsJSON.allTracks[i]);
          this.songservice.get_genres_for_hex(songsJSON.allTracks[i].spotifySongId).subscribe(res =>
          {
            console.log(res);
            hex_arr.push(res.genres);

          });
        return hex_arr;
        }
    }



    fill_table(){
      console.log(get_postgame_songs());
      var postgame_songs = get_postgame_songs();
      var j = 1;
      for(j = 1 ; j < 6; j++){
        document.getElementById("title_"+j).innerText = postgame_songs.allTracks[j-1].songname;
        document.getElementById("interpret_"+j).innerText = postgame_songs.allTracks[j-1].artist;
      }

      function get_postgame_songs(){
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "http://localhost:3000/api/get_Round_Tracks", false);
        xhr.onload = function (e) {
          if (xhr.readyState === 4) {
            if (xhr.status === 200) {
          }   else {
              console.error(xhr.statusText);
            }
          }
        };
        xhr.onerror = function (e) {
          console.error(xhr.statusText);
        };
        xhr.send(null);
        var songsJSON = JSON.parse(xhr.responseText);
        return songsJSON;
      }
    }


  ngOnInit() {
    this.create_hexagon();
    this.fill_table();
  }

}
