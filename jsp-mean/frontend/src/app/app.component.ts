import { Component, NgZone } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private zone:NgZone, private router:Router){}

  title = 'frontend';

  isLoggedIn(){
    var xhr = new XMLHttpRequest();
      xhr.open("GET", "http://localhost:3000/api/get_Logged_In", false);
      xhr.onload = function (e) {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
        }   else {
            console.error(xhr.statusText);
          }
        }
      };
      xhr.onerror = function (e) {
        console.error(xhr.statusText);
      };
      xhr.send(null);
      var response = JSON.parse(xhr.responseText);
      if(response.isLoggedIn){
        this.zone.run(() => this.router.navigate(['/play']));
      }else{
        alert("NOT LOGGED IN!");
      };
  }

  button_switch(){
    var xhr = new XMLHttpRequest();
      xhr.open("GET", "http://localhost:3000/api/get_Logged_In", false);
      xhr.onload = function (e) {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
        }   else {
            console.error(xhr.statusText);
          }
        }
      };
      xhr.onerror = function (e) {
        console.error(xhr.statusText);
      };
      xhr.send(null);
      var response = JSON.parse(xhr.responseText);
      if(response.isLoggedIn){
        return true;
      }else{
        return false;
      };
  }

}
