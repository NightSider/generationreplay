import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class JavascriptService {

  uri = 'http://localhost:3000/js';

constructor(private http: HttpClient) { }
  create_hexagon(){
    return this.http.get(`${this.uri}/gethex`);
  }
}
