import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { SongService } from './song.service';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { PlayhubComponent } from './components/playhub/playhub.component';
import { LeaderboardComponent } from './components/leaderboard/leaderboard.component';
import { PlayComponent } from './components/play/play.component';
import { AdvancedplayComponent } from './components/advancedplay/advancedplay.component';
import { SuccessComponent } from './components/success/success.component';
import { LooseComponent } from './components/loose/loose.component';
import { PostgameComponent } from './components/postgame/postgame.component';
import { LogoutComponent } from './components/logout/logout.component';

const routes: Routes = [
  {
    path: 'advancedplay',
    component: AdvancedplayComponent
  },
  {
    path: 'leaderboard',
    component: LeaderboardComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'playhub',
    component: PlayhubComponent
  },
  {
    path: 'play',
    component: PlayComponent
  },
  {
    path: 'success',
    component: SuccessComponent
  },
  {
    path: 'loose',
    component: LooseComponent
  },
  {
    path: 'postgame',
    component: PostgameComponent
  },
  {
    path: 'logout',
    component: LogoutComponent
  }
];


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PlayhubComponent,
    LeaderboardComponent,
    PlayComponent,
    AdvancedplayComponent,
    SuccessComponent,
    LooseComponent,
    PostgameComponent,
    LogoutComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
  ],
  providers: [ SongService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
