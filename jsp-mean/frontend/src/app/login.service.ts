import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import { Headers } from '@angular/http';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  uri = 'http://localhost:3000/api';

  constructor(private http: HttpClient) { }

  login() {
    this.http.get(`${this.uri}/login`).subscribe(res => console.log('loginservice'));
  }

  logout() {
    this.http.get(`${this.uri}/logout`).subscribe(res => console.log('logoutservice'));
  }

  addSong(songId, genre) {
    var temp_genre = [{name:genre, counter:1}];
    const obj = {
      songId: songId,
      genres: temp_genre
    };
    return this.http.post(`${this.uri}/add`, obj);
  }

  trycors(){
    this.http.get(`${this.uri}/products`).subscribe(res => console.log('try cors'));
  }
}
