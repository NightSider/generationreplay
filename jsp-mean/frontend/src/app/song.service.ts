import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SongService {

  uri = 'http://localhost:3000/song';

  constructor(private http: HttpClient) { }

  /*updateSong(song_name, genre){
    var x = this.http.get(`${this.uri}/readdb/${song_name}`).subscribe(res => console.log('Found song'));
    console.log(x);
    if (x == song_name){
      console.log("geschafft");
    }
    else{
      console.log("nicht geschafft");
    }
  }*/

  findSong(song_get){
    return this.http.get(`${this.uri}/readdb/${song_get}`);
  }


  findAlbumById(album_id){
    this.http.get(`${this.uri}/album/${album_id}`).subscribe(res => console.log('Found albums'));
  }

  get_genres_for_hex(my_json){
    return this.http.get(`${this.uri}/gethexgenres/${my_json}`);
  }

  getUsers(){
    return this.http.get(`${this.uri}/users`);
  }
}
