const express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    mongoose = require('mongoose'),
    config = require('./config/DB');
var cookieParser = require('cookie-parser');

    const app = express();

    mongoose.Promise = global.Promise;
    mongoose.connect(config.DB).then(
      () => {console.log('Database is connected') },
      err => { console.log('Can not connect to the database'+ err)}
    );
    const song_routes = require('./routes/routes');
    const api_routes = require('./routes/SpotifyHTTPReq');
    const script_routes = require('./routes/scripts');
    const port = process.env.PORT || 3000;

    app.use(cors());
    app.use(bodyParser.json());
    app.use(cookieParser())
    app.use('/song', song_routes);
    app.use('/api', api_routes);
    app.use('/js', script_routes);

    const server = app.listen(port, function(){
     console.log('Listening on port ' + port);
    });
