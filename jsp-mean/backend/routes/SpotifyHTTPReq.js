const express = require('express');
const app = express();
const api_routes = express.Router();
const request = require("request");
var cookieParser = require('cookie-parser');
var path    = require("path");

var querystring = require('querystring');
var stateKey = 'spotify_auth_state';
var cors = require('cors')({origin:true});

var client_id = '8ce521702cf345a3a80e7a47409cf121'; // Your client id
var client_secret = '6e9d82a494d7470daea264431df359b9'; // Your secret
var redirect_uri = 'http://localhost:3000/api/callback'; // Your redirect uri
var spotifyPlaylistID = '3IougZus0bDoURAK1MWo8S';
var global_token;
var tracksForRound;
var allTracksForRound;
var guessed = 0;
var flaggy = false;

// Require AdUnit model in our routes module
let Song = require('../models/song');
let User = require('../models/user');


var generateRandomString = function(length) {
   var text = '';
   var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

   for (var i = 0; i < length; i++) {
     text += possible.charAt(Math.floor(Math.random() * possible.length));
   }
  return text;
};

var setToken = function(token){
  global_token=token;
}

var getToken = function(){
  return global_token;
}

api_routes.route('/close_login').get(function(req, res) {
  res.send('<script>window.close()</script>');
  // Todo: handle successfull login here
})

api_routes.get('/login', function (req, res){
    var state = generateRandomString(16);
    res.cookie(stateKey, state);
    console.log("statekey: "+stateKey);
    var scope = 'user-read-private user-read-email';
    res.redirect('https://accounts.spotify.com/authorize?' +
      querystring.stringify({
        response_type: 'code',
        client_id: client_id,
        scope: scope,
        redirect_uri: redirect_uri,
        state: state
      }));
});

api_routes.get('/logout', function (req, res){
    res.redirect('https://accounts.spotify.com/logout');
    setToken(null);
    console.log("empty token: "+global_token);
});


api_routes.route('/callback').get(function (req, res){
    var code = req.query.code || null;
    var state = req.query.state || null;
    var storedState = req.cookies ? req.cookies[stateKey] : null;

    if (state === null || state !== storedState) {
      res.redirect('/#' +
        querystring.stringify({
          error: 'state_mismatch'
        }));
    } else {
      res.clearCookie(stateKey);
      var authOptions = {
        url: 'https://accounts.spotify.com/api/token',
        form: {
          code: code,
          redirect_uri: redirect_uri,
          grant_type: 'authorization_code'
        },
        headers: {
          'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
        },
        json: true
      };

      request.post(authOptions, function(error, response, body) {
        if (!error && response.statusCode === 200) {
          var access_token = body.access_token,
              refresh_token = body.refresh_token;
          setToken(body.access_token);

          var options = {
            url: 'https://api.spotify.com/v1/me',
            headers: { 'Authorization': 'Bearer ' + access_token },
            json: true
          };

          res.redirect('/api/close_login');
        } else {
          res.redirect('/#' +
            querystring.stringify({
              error: 'invalid_token'
            }));
        }
      });
    }
});

api_routes.route('/refresh_token').get(function (req, res){
    // requesting access token from refresh token
    var refresh_token = req.query.refresh_token;
    var authOptions = {
      url: 'https://accounts.spotify.com/api/token',
      headers: { 'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64')) },
      form: {
        grant_type: 'refresh_token',
        refresh_token: refresh_token
      },
      json: true
    };

    request.post(authOptions, function(error, response, body) {
      if (!error && response.statusCode === 200) {
        var access_token = body.access_token;
        res.send({
          'access_token': access_token
        });
      }
    });
});

api_routes.get('/next_track', function(req, res, next){
  var playlistForm = {
    url: 'https://api.spotify.com/v1/playlists/' + spotifyPlaylistID + '/tracks',
    headers: {
      'Authorization': 'Bearer ' + getToken()
    },
    json: true
  }
  request.get(playlistForm, function(reqS, resS, body){
    var songURIs = [];
    var amountOfItems = (body.items).length;
    for(itemIndex = 0; itemIndex < amountOfItems; itemIndex++){
      var item = {
          artist: body.items[itemIndex].track.artists[0].name,
          songname: body.items[itemIndex].track.name,
          uri: body.items[itemIndex].track.preview_url,
          spotifySongId: body.items[itemIndex].track.id
      };
      songURIs.push(item);
    }
    res.send({
      statusCode: 200,
      songURIs: songURIs
    })
    console.log("Response was sent to client with " + songURIs.length + " items.")
  })
})

api_routes.post('/change_Round_Tracks', function(req, res, next){
  if(req.body.length==5){
    allTracksForRound = req.body
  }
  tracksForRound = req.body;
  res.send({
    statusCode: 200
  })
})

api_routes.post('/guessed', function(req, res, next){
  guessed++;
  if(guessed==5){
    guessed=0;
    flaggy=true;
  }else{
    flaggy=false;
  }
  res.send({
    statusCode: 200,
  })
})

api_routes.get('/get_guessed', function(req, res, next){
  res.send({
    statusCode: 200,
    flag: flaggy
  })
  flaggy=false;
})

api_routes.get('/get_Round_Tracks', function(req, res, next){
  res.send({
    statusCode: 200,
    tracks: tracksForRound,
    allTracks: allTracksForRound
  })
})

api_routes.get('/get_Logged_In', function(req, res, next){
  //console.log("Token: " + getToken())
  var isTokenSet = false;
  if(getToken()){
    isTokenSet = true;
  }
  res.send({
    statusCode: 200,
    isLoggedIn: isTokenSet
  })
})

//This is for testing: Just call localhost:3000/api/products
api_routes.get('/products', function (req, response, next) {
  // res.json({msg: 'My Token is:'})
  // console.log(getToken());
  var test = getSongInfosFromPlaylist(spotifyPlaylistID)
  function asdf(){
    request.get(test, function(req,res,body){
      console.log(body);
      var songURIs = []
      for(itemIndex = 0; itemIndex < body.items.length; itemIndex++){
        var item = {
          artist: body.items[itemIndex].track.artists[0].name,
          songname: body.items[itemIndex].track.name,
          uri: body.items[itemIndex].track.preview_url,
        }
        songURIs.push(item);
      }
      console.log('return songuris')
      response.send(songURIs);
      return songURIs;
    })
  }
  asdf();
})

function updateUserScore(guess){
  var url = 'https://api.spotify.com/v1/me';
  var form = {
    url: url,
    headers: { 'Authorization': 'Bearer ' + getToken()},
    json: true
  };

  request.get(form, (err, res, body) => {
    if(err){
      console.log(err);
    }
    else{
      var username = body.id;
      console.log(body);
      User.find({userName: username}, (err, res1) => {
        if(err){
          console.log(err);
        }
        else if(res1.length == 0){
        console.log("user find length == 0");
        let user = new User();
        user.userName = username;
        user.guessesRight = 0;
        user.guessesWrong = 0;
        user.qualified = false;

        if(guess == true){
          user.guessesRight = 1;
        }
        else{
          user.guessesWrong = 1;
        }
        user.save()
          .then(user => {
          console.log("new user added");
          })
          .catch(err => {
          console.log("unable to save user to database");
          console.log(err);
        });
      }
      else{
        console.log(res1);
        var user = res1[0];
        if(guess == true){

          User.updateOne({userName: user.userName}, {guessesRight: user.guessesRight + 1}, (err, res2) => {
            if(err){
              console.log(err);
            }
            else{
              console.log("user guesses right updated in DB");
            }
          });
        }
        else{
          User.updateOne({userName: user.userName}, {guessesWrong: user.guessesWrong + 1}, (err, res2) => {
            if(err){
              console.log(err);
            }
            else{
              console.log("user guesses false updated in DB");
            }
          });
        }
      }
    });
}});
}

api_routes.route('/add').post((req, res) =>{
  let song = new Song(req.body);
  var songId = song.songId;

  var genreArrNew = song.genres;//array mit dem genre-objekt, das geraten wurde
  Song.find({songId: songId}, (err, res1) => {
    if(err){
      console.log(err);
    }
    else if(res1.length == 0){
      console.log("new song added");
      console.log(song);
      var spotify_genre = getSpotifyGenre(songId, song);
    }
    else{
      var genreArrOld = res1[0].genres;
      genreArrNew.map(genreNew => {
        var existing = genreArrOld.find(genreOld => genreOld.name == genreNew.name);

        //if genre already exists, counter++, else add new genre
        if(existing != undefined){
          genreArrOld.find(genreOld => genreOld.name == genreNew.name).counter++;
        }
        else{
          genreArrOld.push(genreArrNew[0]); //song.genres is ein einelementiges array, wir wollen nur das genre-objekt haben
        }
      });
      console.log(genreArrOld);

      Song.updateOne({_id : res1[0]._id}, {genres: genreArrOld},(err, res2) => {
        if(err){
          console.log(err);
        }
        else{
          console.log("updated in DB");
        }
      });

      var th = 0.6;//treshold to decide if the genre is correct
      var genreGuessed = genreArrNew[0].name;
      var counterAllGenres = genreArrOld.reduce((acc, obj) => (acc + obj.counter), 0);//adds all counters of each genre together
      var fq = genreArrOld.find(genre => genre.name == genreGuessed).counter / counterAllGenres;//calculates the frequency of the guessed genre
      if (fq > th){
        updateUserScore(true);
        res.json({guess: true});
      }
      else{
        updateUserScore(false);
        res.json({guess: false});
      }
    }
  });
});

var getSpotifyGenre = function(songId, song){
  var artistForm = {
    	url: 'https://api.spotify.com/v1/tracks/'+ songId,
      headers: {
        'Authorization': 'Bearer ' + getToken()
      },
      json: true
  }
  request.get(artistForm, (err, res, body) => {
    if(err){
      console.log(err);
    }else{
      var artist_name = body.artists[0].id;
      console.log("-----------NEW SONG: "+ body.artists[0].name);
      var genreForm = {
        url: 'https://api.spotify.com/v1/artists/'+artist_name,
        headers: {
          'Authorization': 'Bearer ' + getToken()
        },
        json: true
      }
      request.get(genreForm, (err1, res1, body1) => {
        if(err1){
          console.log(err1);
        }else{
          var genre_array = body1.genres;
          var matched_genres = matchDBgenres(genre_array);
          var base_genres = matched_genres.map(genre => { return {name: genre, counter: 10}});
          var guessed_by_user = song.genres[0].name;
          console.log("guessed by user: "+guessed_by_user);
          // base_genres = matched_genres.map(genre => {console.log("genrename "+genre);if (guessed_by_user==genre){
          //                                             return {name: genre, counter: 11}
          //                                           }else{
          //                                             return {name: genre, counter: 10}
          //                                           }});
          var genres_full = base_genres.map(genre => {
            if(genre.name == guessed_by_user){
              console.log("GLEICH 11" + genre.name );
                return {name: genre.name, counter: 11}
            }
            else{
              console.log("NICHT GLEICH " + genre.name);
              return {name: genre.name, counter: 10}
            }
          });
          genres_full = genres_full.concat(song.genres);
          //song.genres = base_genres;
          song.genres = genres_full;
          console.log("-----------NEW SONG: GENRES "+ base_genres);
          console.log("---------SONG ID: " + song.songId);
          //console.log("---------SONG GENRE: " + song.genres[0]);
          song.genres.map(genre => console.log(genre));
          song.save()
            .then(game => {
            console.log("new song added");
            })
            .catch(err => {
            console.log("unable to save to database");
            console.log(err);
          });
        }
    });
}});
}


var matchDBgenres = function(genre_array){
  var return_list = [];
  var own_genres = ["Pop", "Pop Punk", "Rap", "Trap", "Pop Rock", "Alternative Rock", "Classic Rock", "Classic Pop", "Latin Pop", "Indie Pop", "Alternative Metal", "Rock", "Grunge", "Hard Rock", "Reggaeton", "Indie Folk", "Electro House", "Nu Metal", "Gangster Rap", "Garage Rock", "Folk Rock", "Soul", "Underground Hip-Hop", "Psychedelic Rock", "Hip-Hop", "R&B", "EDM", "Latin", "Electro", "Metal", "Death Metal", "Hardcore", "Metalcore", "Deathcore", "Deep House", "Acoustic Pop", "Rock&Roll", "Folk", "Country", "Punk", "Indie", "Techno", "Funk", "Blues", "Jazz", "Classical", "House", "Singer-Songwriter", "Alternative", "Swing", "Reggae", "Schlager", "Disco", "Trance", "Ska", "Drum&Bass", "Hardstyle", "Viking Metal", "Thrash Metal", "Black Metal", "Power Metal", "Punk Rock"];
  var lc_own_genres = own_genres.map(genre => {return  genre.toLowerCase()});

  for (var i = 0; i < genre_array.length; i++){
    for (var j = 0; j < lc_own_genres.length; j++){
      if(genre_array[i].includes(lc_own_genres[j])){
        return_list.push(own_genres[j]);
      }
    }
    return return_list;
  }


}


// Function works, but no possible call from other location, because returned value is always undefined
var getSongInfosFromPlaylist = function(playlistID){
  var playlistForm = {
    url: 'https://api.spotify.com/v1/playlists/' + playlistID + '/tracks',
    headers: {
      'Authorization': 'Bearer ' + getToken()
    },
    json: true
  }
  return playlistForm;
}

module.exports = api_routes;
