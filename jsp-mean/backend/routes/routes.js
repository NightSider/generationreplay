// adunit.route.js

const express = require('express');
const app = express();
const song_routes = express.Router();
// frameworks
const request = require('request');

// Require AdUnit model in our routes module
let Song = require('../models/song');
let User = require('../models/user');

song_routes.route('/gethexgenres/:id').get(function (req, res){
  var songid = req.params.id;
  console.log(songid);
  Song.find({songId: songid}, (err, song) =>{
    if(err){
      console.log("nope");
      console.log(err);
    }else{
      var genres = song[0].genres;
      genres.sort((a, b) => {
        if(a.counter > b.counter){
          return -1;
        }
        else if(a.counter < b.counter){
          return 1;
        }
        else{
          return 0;
        }
      });
      while(genres.length < 6){
        genres.push(0);
      }
      genres = genres.slice(0,6);
      console.log(genres);
    }
    res.send({genres: genres});
});
});

song_routes.route('/readdb/:name').get(function (req, res){
  var name = req.params.name;
  Song.findOne({songId: name}, (err, song) =>{
    if(err){
      console.log("nope");
      console.log(err);
    }
    else{
      if(song != null){
        res.json({songInDB: true});
      }
      else{
        res.json({songInDB: false});
      }
    }
  });
});

song_routes.route('/album/:id').get(function (req, res){
  var id = req.params.id;
  console.log(id);
  var url = 'https://api.spotify.com/v1/artists/'+id+'/albums';
  var form = {
    url: url,
    headers: { 'Authorization': 'Bearer ' + 'BQDTxM5mlaxOSUJ6XeiEXaTmT9zpOPWg0MQ1EzWvdwW4zi95TB' },
    json: true
  };
  request.get(form, function (err, res, body){
    console.log(body);
  })
});

song_routes.route('/users').get((req, res) => {
  User.find({}, (err, res1) =>{
    if(err || res1.length == 0){
      console.log("err oder res==0");
    }
    else{
      userList = [];
      for(i = 0; i < res1.length; i++){
        var kdr = res1[i].guessesRight / (res1[i].guessesRight + res1[i].guessesWrong);
        var user = {
          userName: res1[i].userName,
          ratio: kdr,
          guessesRight: res1[i].guessesRight,
          guessesWrong: res1[i].guessesWrong
        }
        userList.push(user);
      }
      res.send({users: userList});
    }
  })
});


/*song_routes.route('/update').get(function (req, res){

});*/

module.exports = song_routes;
