const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var genres = {_id: false, name: String, counter: Number};

let Song = new Schema({
  songId: {
    type: String,
    unique: true
  },
  genres: [genres]
},{
    collection: 'songs'
});
module.exports = mongoose.model('Song', Song);
