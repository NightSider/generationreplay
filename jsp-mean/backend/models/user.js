const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let User = new Schema({
  userName: {type: String, unique: true},
  guessesRight: Number,
  guessesWrong: Number,
  qualified: Boolean,
},{
    collection: 'users'
});
module.exports = mongoose.model('User', User);
